package com.giorgiofellipe.pizzaprinter.Thread;

/**
 * Created by Raymond on 5/3/2018.
 */

public class WatchThread extends Thread {
  ConnectThread thread;
  long time;

  public WatchThread(ConnectThread inputThread, long inputTime) {
    thread = inputThread;
    time = inputTime;
  }

  public void triggered() {
    System.out.println("[PizzaPrinter][WatchThread] Trigger ran.");

    thread.fail(new Exception("Timeout"));
    thread.interrupt();
  }

  public void run() {
    long startTime = System.currentTimeMillis();

    while (true) {
      if ( Thread.interrupted() || !thread.isAlive()) {
        System.out.println("[PizzaPrinter][WatchThread] The watcher thread has been stopped.");
        break;
      }

      long curTime = System.currentTimeMillis();

      if ( (curTime - time) > startTime ) {
        triggered();
        break;
      }
    }
  }
}
