package com.giorgiofellipe.pizzaprinter;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;

import com.giorgiofellipe.pizzaprinter.Sockets.NormalSocket;
import com.giorgiofellipe.pizzaprinter.Sockets.ServiceRecordSocket;
import com.giorgiofellipe.pizzaprinter.Sockets.SocketType;
import com.giorgiofellipe.pizzaprinter.Thread.ConnectThread;

/**
 * Created by Raymond on 5/3/2018.
 */

public class SocketWorker {
  public SocketType[] socketTypes = {
    new ServiceRecordSocket()
  };

  public int socketIndex = 0;

  public SocketType getNextSocket() {
    if ( socketIndex >= socketTypes.length ) {
      return null;
    }

    return socketTypes[ socketIndex ];
  }

  public void getSocket(final BluetoothDevice device, final SocketResponse response) {
    ConnectThread connectThread = new ConnectThread(device, getNextSocket()) {
      @Override
      public void onConnected(BluetoothSocket socket) {
        response.connected(socket);
      }

      @Override
      public void onFailed() {
        socketIndex++;

        if ( getNextSocket() == null ) {
          response.failed( exception );
        } else {
          getSocket(device, response);
        }
      }
    };

    connectThread.begin();
  }
}
