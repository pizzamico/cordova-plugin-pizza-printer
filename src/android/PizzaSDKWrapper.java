package com.giorgiofellipe.pizzaprinter;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaWebView;
import org.json.JSONObject;
import org.json.JSONArray;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Exception;
import java.net.InetAddress;
import java.util.Hashtable;
import java.util.Map;
import java.util.HashMap;
import java.util.Set;
import java.lang.reflect.Method;

import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.os.Build;
import android.util.Log;
import android.content.Intent;
import android.os.Bundle;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Base64;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.datecs.api.printer.ProtocolAdapter;
import com.giorgiofellipe.pizzaprinter.Thread.TimerThread;

public class PizzaSDKWrapper {
  private static final String LOG_TAG = "BluetoothPrinter";
  private Printer mPrinter;
  private ProtocolAdapter mProtocolAdapter;
  private BluetoothSocket mBluetoothSocket;
  private boolean mRestart;
  public String mAddress;
  private CallbackContext mConnectCallbackContext;
  private CallbackContext mCallbackContext;
  private CordovaInterface mCordova;
  private CordovaWebView mWebView;
  private final Application app;

  /**
   * Interface de eventos da Impressora
   */
  private final ProtocolAdapter.PrinterListener mChannelListener = new ProtocolAdapter.PrinterListener() {
    @Override
    public void onPaperStateChanged(boolean hasNoPaper) {
      if (hasNoPaper) {
        sendStatusUpdate(true, false);
        showToast(PizzaUtil.getStringFromStringResource(app, "no_paper"));
      } else {
        sendStatusUpdate(true, true);
        showToast(PizzaUtil.getStringFromStringResource(app, "paper_ok"));
      }
    }

    @Override
    public void onThermalHeadStateChanged(boolean overheated) {
      if (overheated) {
        closeActiveConnections();
        sendStatusUpdate(false, false);
        showToast(PizzaUtil.getStringFromStringResource(app, "overheating"));
      }
    }

    @Override
    public void onBatteryStateChanged(boolean lowBattery) {
      if (lowBattery) {
        sendStatusUpdate(true, true, false);
        showToast(PizzaUtil.getStringFromStringResource(app, "low_battery"));
      }
    }
  };

  private Map<Integer, String> errorCode = new HashMap<Integer, String>();

  public PizzaSDKWrapper(CordovaInterface cordova) {
    mCordova = cordova;

    app = cordova.getActivity().getApplication();

    this.errorCode.put(1, PizzaUtil.getStringFromStringResource(app, "err_no_bt_adapter"));
    this.errorCode.put(2, PizzaUtil.getStringFromStringResource(app, "err_no_bt_device"));
    this.errorCode.put(3, PizzaUtil.getStringFromStringResource(app, "err_lines_number"));
    this.errorCode.put(4, PizzaUtil.getStringFromStringResource(app, "err_feed_paper"));
    this.errorCode.put(5, PizzaUtil.getStringFromStringResource(app, "err_print"));
    this.errorCode.put(6, PizzaUtil.getStringFromStringResource(app, "err_fetch_st"));
    this.errorCode.put(7, PizzaUtil.getStringFromStringResource(app, "err_fetch_tmp"));
    this.errorCode.put(8, PizzaUtil.getStringFromStringResource(app, "err_print_barcode"));
    this.errorCode.put(9, PizzaUtil.getStringFromStringResource(app, "err_print_test"));
    this.errorCode.put(10, PizzaUtil.getStringFromStringResource(app, "err_set_barcode"));
    this.errorCode.put(11, PizzaUtil.getStringFromStringResource(app, "err_print_img"));
    this.errorCode.put(12, PizzaUtil.getStringFromStringResource(app, "err_print_rect"));
    this.errorCode.put(13, PizzaUtil.getStringFromStringResource(app, "err_print_rect"));
    this.errorCode.put(14, PizzaUtil.getStringFromStringResource(app, "err_print_rect"));
    this.errorCode.put(15, PizzaUtil.getStringFromStringResource(app, "err_print_rect"));
    this.errorCode.put(16, PizzaUtil.getStringFromStringResource(app, "err_print_rect"));
    this.errorCode.put(17, PizzaUtil.getStringFromStringResource(app, "err_print_rect"));
    this.errorCode.put(18, PizzaUtil.getStringFromStringResource(app, "failed_to_connect"));
    this.errorCode.put(19, PizzaUtil.getStringFromStringResource(app, "err_bt_socket"));
    this.errorCode.put(20, PizzaUtil.getStringFromStringResource(app, "failed_to_initialize"));
    this.errorCode.put(21, PizzaUtil.getStringFromStringResource(app, "err_write"));
    this.errorCode.put(22, PizzaUtil.getStringFromStringResource(app, "err_print_qrcode"));
  }

  private JSONObject getErrorByCode(int code) {
    return this.getErrorByCode(code, null);
  }

  private JSONObject getErrorByCode(int code, Exception exception) {
    JSONObject json = new JSONObject();
    try {
      json.put("errorCode", code);
      json.put("message", errorCode.get(code));
      if (exception != null) {
        json.put("exception", exception.getMessage());
      }
    } catch (Exception e) {
      Log.e(LOG_TAG, e.getMessage());
      showToast(e.getMessage());
    }
    return json;
  }

  /**
   * Busca todos os dispositivos Bluetooth pareados com o device
   *
   * @param callbackContext
   */

  BroadcastReceiver onlineReceiver = null;

  protected void isOnline(final String address, final CallbackContext callbackContext) {
    final BluetoothAdapter mBTA = BluetoothAdapter.getDefaultAdapter();

    //check to see if there is BT on the Android device at all
    if (mBTA == null){
      callbackContext.error("No BlueTooth Adapter");
      return;
    }

    if (!mBTA.isEnabled()){
      callbackContext.error("BlueTooth not enabled.");
      return;
    }

    if ( onlineReceiver != null ) {
      this.mCordova.getActivity().unregisterReceiver(onlineReceiver);
    }

    //cancel any prior BT device discovery
    if (mBTA.isDiscovering()){
      mBTA.cancelDiscovery();
    }

    //re-start discovery
    mBTA.startDiscovery();

    //let's make a broadcast receiver to register our things
    final BroadcastReceiver mReceiver = new BroadcastReceiver() {
      public boolean finished = false;

      public void onReceive(Context context, Intent intent) {
        if ( finished || callbackContext.isFinished() ) {
          return;
        }

        String action = intent.getAction(); //may need to chain this to a recognizing function

        if (BluetoothDevice.ACTION_FOUND.equals(action)){
          // Get the BluetoothDevice object from the Intent
          BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

          if ( device.getAddress().toLowerCase().equals(address.toLowerCase()) ) {
            callbackContext.success();
            mBTA.cancelDiscovery();

            finished = true;
          }
        }
      }
    };

    new TimerThread(10) {
      @Override
      public void timeOver() {
        if ( !callbackContext.isFinished() ) {
          System.out.print("[PIZZAPRINTER] Delete timeover");
          callbackContext.error("Time over");
          mBTA.cancelDiscovery();
        }
      }
    }.start();

    onlineReceiver = mReceiver;

    IntentFilter ifilter = new IntentFilter(BluetoothDevice.ACTION_FOUND);

    this.mCordova.getActivity().registerReceiver(mReceiver, ifilter);
  }

  protected void getBluetoothPairedDevices(CallbackContext callbackContext) {
    BluetoothAdapter mBluetoothAdapter = null;
    try {
      mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
      if (mBluetoothAdapter == null) {
        callbackContext.error(this.getErrorByCode(1));
        return;
      }
      if (!mBluetoothAdapter.isEnabled()) {
        Intent enableBluetooth = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
        this.mCordova.getActivity().startActivityForResult(enableBluetooth, 0);
      }
      Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
      if (pairedDevices.size() > 0) {
        JSONArray json = new JSONArray();
        for (BluetoothDevice device : pairedDevices) {
          Hashtable map = new Hashtable();
          int deviceType = 0;
          try {
            Method method = device.getClass().getMethod("getType");
            if (method != null) {
              deviceType = (Integer) method.invoke(device);
            }
          } catch (Exception e) {
            e.printStackTrace();
          }
          map.put("type", deviceType);
          map.put("address", device.getAddress());
          map.put("name", device.getName());
          String deviceAlias = device.getName();
          try {
            Method method = device.getClass().getMethod("getAliasName");
            if (method != null) {
              deviceAlias = (String) method.invoke(device);
            }
          } catch (Exception e) {
            e.printStackTrace();
          }
          map.put("aliasName", deviceAlias);
          JSONObject jObj = new JSONObject(map);
          json.put(jObj);
        }
        callbackContext.success(json);
      } else {
        callbackContext.error(this.getErrorByCode(2));
      }
    } catch (Exception e) {
      Log.e(LOG_TAG, e.getMessage());
      e.printStackTrace();
      callbackContext.error(e.getMessage());
    }
  }

  /**
   * Seta em memória o endereço da impressora cuja conexão está sendo estabelecida
   *
   * @param address
   */
  protected void setAddress(String address) {
    mAddress = address;
  }

  protected void setWebView(CordovaWebView webView) {
    mWebView = webView;
  }

  // public void setCordova(CordovaInterface cordova) {
  //     mCordova = cordova;
  // }

  /**
   * CallbackContext de cada requisição, que efetivamente recebe os retornos dos métodos
   *
   * @param callbackContext
   */
  public void setCallbackContext(CallbackContext callbackContext) {
    mCallbackContext = callbackContext;
  }

  /**
   * Valida o endereço da impressora e efetua a conexão
   *
   * @param callbackContext
   */

  protected void connect(CallbackContext callbackContext) {
    mConnectCallbackContext = callbackContext;

    closeActiveConnections();

    if (BluetoothAdapter.checkBluetoothAddress(mAddress)) {
      establishBluetoothConnection(mAddress, callbackContext);
    }
  }

  /**
   * Encerra todas as conexões com impressoras e dispositivos Bluetooth ativas
   */
  public synchronized void closeActiveConnections() {
    closePrinterConnection();
    closeBluetoothConnection();

    try {
      Thread.sleep(500);
    } catch (Exception e) {

    }
  }

  /**
   * Encerra a conexão com a impressora
   */
  private synchronized void closePrinterConnection() {
    if (mPrinter != null) {
      mPrinter.close();
    }

    if (mProtocolAdapter != null) {
      mProtocolAdapter.close();
    }
  }

  /**
   * Finaliza o socket Bluetooth e encerra todas as conexões
   */
  private synchronized void closeBluetoothConnection() {
    try {
      if (mBluetoothSocket != null) {
        InputStream input = mBluetoothSocket.getInputStream();
        OutputStream output = mBluetoothSocket.getOutputStream();

        if (input != null) {
          input.close();
        }

        if (output != null) {
          output.close();
        }

        Thread.sleep(50);

        mBluetoothSocket.close();
        mBluetoothSocket = null;
      }
    } catch (Exception e) {

    }
  }

  /**
   * Efetiva a conexão com o dispositivo Bluetooth
   *
   * @param address
   * @param callbackContext
   */

   public void restartDevice() {
       try {
         Runtime.getRuntime().exec(new String[]{"su","-c","reboot now"});
       } catch (IOException e) {
         e.printStackTrace();
       }
    }

  public void restartBluetooth() {
    BluetoothAdapter adapter = BluetoothAdapter.getDefaultAdapter();

    adapter.disable();

    while ( adapter.isEnabled() ) {
      try {
        Thread.sleep(500);
        adapter.disable();
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }

    adapter.enable();

    while ( !adapter.isEnabled() ) {
      try {
        Thread.sleep(500);
        adapter.enable();
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
  }

  private void establishBluetoothConnection(final String address, final CallbackContext callbackContext) {
    BluetoothAdapter adapter = BluetoothAdapter.getDefaultAdapter();
    BluetoothDevice device = adapter.getRemoteDevice(address);

    // This response will be triggered either when all the sockets the worker can attempt
    // have failed or we found one successful socket.
    SocketResponse response = new SocketResponse() {
      @Override
      public void connected(BluetoothSocket socket) {
        mBluetoothSocket = socket;

        System.out.println("[LMAO] WE ARE CONNECTED: " + mAddress);

        try {
          initializePrinter(socket.getInputStream(), socket.getOutputStream(), callbackContext);
          sendStatusUpdate(true);
        } catch (IOException e) {
          failed(new Exception("Printer could not initialize"));
        }
      }

      @Override
      public void failed(Exception exception) {
        System.out.println("[LMAO] WE ARE NOT CONNECTED: " + mAddress);

        callbackContext.error( exception.getMessage() );
      }
    };

    System.out.println("[LMAO] starting socket creation: " + mAddress);

    // This creates our threads.
    SocketWorker socketWorker = new SocketWorker();
    socketWorker.getSocket(device, response);
  }

  /**
   * Cria um socket Bluetooth
   *
   * @param device
   * @param uuid
   * @param callbackContext
   * @return BluetoothSocket
   * @throws IOException
   */

  /**
   * Inicializa a troca de dados com a impressora
   * @param inputStream
   * @param outputStream
   * @param callbackContext
   * @throws IOException
   */
  protected void initializePrinter(InputStream inputStream, OutputStream outputStream, CallbackContext callbackContext) throws IOException {
    mProtocolAdapter = new ProtocolAdapter(inputStream, outputStream);
    if (mProtocolAdapter.isProtocolEnabled()) {
      mProtocolAdapter.setPrinterListener(mChannelListener);

      final ProtocolAdapter.Channel channel = mProtocolAdapter.getChannel(ProtocolAdapter.CHANNEL_PRINTER);

      mPrinter = new Printer(channel.getInputStream(), channel.getOutputStream());
    } else {
      mPrinter = new Printer(mProtocolAdapter.getRawInputStream(), mProtocolAdapter.getRawOutputStream());
    }


    mPrinter.setConnectionListener(new Printer.ConnectionListener() {
      @Override
      public void onDisconnect() {
        sendStatusUpdate(false);
      }
    });
    callbackContext.success();
  }

  /**
   * Alimenta papel à impressora (rola papel em branco)
   *
   * @param linesQuantity
   */
  public void feedPaper(int linesQuantity) {
    if (linesQuantity < 0 || linesQuantity > 255) {
      mCallbackContext.error(this.getErrorByCode(3));
    }
    try {
      mPrinter.feedPaper(linesQuantity);
      mPrinter.flush();
      mCallbackContext.success();
    } catch (Exception e) {
      mCallbackContext.error(this.getErrorByCode(4, e));
    }
  }

  /**
   * Print text expecting markup formatting tags (default encoding is ISO-8859-1)
   *
   * @param text
   */
  public void printTaggedText(String text) {
    printTaggedText(text, "ISO-8859-1");
  }

  /**
   * Print text expecting markup formatting tags and a defined charset
   *
   * @param text
   * @param charset
   */
  public void printTaggedText(String text, String charset) {
    try {
      mPrinter.printTaggedText(text, charset);
      mPrinter.flush();
      mCallbackContext.success();
    } catch (Exception e) {
      mCallbackContext.error(this.getErrorByCode(5, e));
    }
  }

  /**
   * Converts HEX String into byte array and write
   *
   * @param
   */
  public void writeHex(String s) {
    write(PizzaUtil.hexStringToByteArray(s));
  }

  /**
   * Writes all bytes from the specified byte array to this printer
   *
   * @param
   */
  public void write(byte[] b) {
    try {
      mPrinter.write(b);
      mPrinter.flush();
      mCallbackContext.success();
    } catch (Exception e) {
      mCallbackContext.error(this.getErrorByCode(21, e));
    }
  }

  /**
   * Return what is the Printer current status
   */
  public void getStatus() {
    try {
      int status = mPrinter.getStatus();
      mCallbackContext.success(status);
    } catch (Exception e) {
      mCallbackContext.error(this.getErrorByCode(6, e));
    }
  }

  /**
   * Return Printer's head temperature
   */
  public void getTemperature() {
    try {
      int temperature = mPrinter.getTemperature();
      mCallbackContext.success(temperature);
    } catch (Exception e) {
      mCallbackContext.error(this.getErrorByCode(7, e));
    }
  }

  public void setBarcode(int align, boolean small, int scale, int hri, int height) {
    try {
      mPrinter.setBarcode(align, small, scale, hri, height);
      mCallbackContext.success();
    } catch (Exception e) {
      mCallbackContext.error(this.getErrorByCode(10, e));
    }
  }

  /**
   * Print a Barcode
   *
   * @param type
   * @param data
   */
  public void printBarcode(int type, String data) {
    try {
      mPrinter.printBarcode(type, data);
      mPrinter.flush();
      mCallbackContext.success();
    } catch (Exception e) {
      mCallbackContext.error(this.getErrorByCode(8, e));
    }
  }

  /**
   * Print a QRCode
   *
   * @param size - the size of symbol, value in {1, 4, 6, 8, 10, 12, 14}
   * @param eccLv - the error collection control level, where 1: L (7%), 2: M (15%), 3: Q (25%), 4: H (30%)
   * @param data - the QRCode data. The data must be between 1 and 448 symbols long.
   */
  public void printQRCode(int size, int eccLv, String data) {
    try {
      mPrinter.printQRCode(size, eccLv, data);
      mPrinter.flush();
      mCallbackContext.success();
    } catch (Exception e) {
      mCallbackContext.error(this.getErrorByCode(22, e));
    }
  }


  /**
   * Print a selftest page
   */
  public void printSelfTest() {
    try {
      mPrinter.printSelfTest();
      mPrinter.flush();
      mCallbackContext.success();
    } catch (Exception e) {
      mCallbackContext.error(this.getErrorByCode(9, e));
    }
  }

  public void drawPageRectangle(int x, int y, int width, int height, int fillMode) {
    try {
      mPrinter.drawPageRectangle(x, y, width, height, fillMode);
      mCallbackContext.success();
    } catch (Exception e) {
      mCallbackContext.error(this.getErrorByCode(12, e));
    }
  }

  public void drawPageFrame(int x, int y, int width, int height, int fillMode, int thickness) {
    try {
      mPrinter.drawPageFrame(x, y, width, height, fillMode, thickness);
      mCallbackContext.success();
    } catch (Exception e) {
      mCallbackContext.error(this.getErrorByCode(16, e));
    }
  }

  public void selectStandardMode() {
    try {
      mPrinter.selectStandardMode();
      mCallbackContext.success();
    } catch (Exception e) {
      mCallbackContext.error(this.getErrorByCode(13, e));
    }
  }

  public void selectPageMode() {
    try {
      mPrinter.selectPageMode();
      mCallbackContext.success();
    } catch (Exception e) {
      mCallbackContext.error(this.getErrorByCode(14, e));
    }
  }

  public void printPage() {
    try {
      mPrinter.printPage();
      mPrinter.flush();
      mCallbackContext.success();
    } catch (Exception e) {
      mCallbackContext.error(this.getErrorByCode(17, e));
    }
  }

  public int getBatteryPercentage(Context context) {

    IntentFilter iFilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
    Intent batteryStatus = context.registerReceiver(null, iFilter);

    int level = batteryStatus != null ? batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1) : -1;
    int scale = batteryStatus != null ? batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, -1) : -1;

    float batteryPct = level / (float) scale;

    return (int) (batteryPct * 100);
  }

  public boolean isCharging(Context context) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
      BatteryManager batteryManager = (BatteryManager) context.getSystemService(Context.BATTERY_SERVICE);
      return batteryManager.isCharging();
    } else {
      IntentFilter filter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
      Intent intent = context.registerReceiver(null, filter);

      int status = intent.getIntExtra(BatteryManager.EXTRA_STATUS, -1);

      if (status == BatteryManager.BATTERY_STATUS_CHARGING || status == BatteryManager.BATTERY_STATUS_FULL) {
        return true;
      }

      int chargePlug = intent.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1);

      if (chargePlug == BatteryManager.BATTERY_PLUGGED_AC || chargePlug == BatteryManager.BATTERY_PLUGGED_USB || chargePlug == BatteryManager.BATTERY_PLUGGED_WIRELESS ) {
        return true;
      }
    }
    return false;
  }

  public boolean isInternetConnected() {
    try {
      InetAddress address = InetAddress.getByName("www.google.com");
      //Connected to working internet connection

      return true;
    } catch (Exception e) {
      return false;
    }
  }

  public void setPageRegion(int x, int y, int width, int height, int direction) {
    try {
      mPrinter.setPageRegion(x, y, width, height, direction);
      mCallbackContext.success();
    } catch (Exception e) {
      mCallbackContext.error(this.getErrorByCode(15, e));
    }
  }


  /**
   * Print an image
   *
   * @param image String (BASE64 encoded image)
   * @param width
   * @param height
   * @param align
   */

  private class ImageWatch {
    public volatile boolean finished = false;
  }

  final ImageWatch imageWatch = new ImageWatch();

  public void printImage(String image, int width, int height, int align) {
    imageWatch.finished = false;

    Thread watchImage = new Thread() {
      @Override
      public void run() {
        long stamp = System.currentTimeMillis();

        while ( !imageWatch.finished && !Thread.interrupted() ) {
          if ( stamp < System.currentTimeMillis() - 60000 ) {
            imageWatch.finished = true;

            if ( !mCallbackContext.isFinished() ) {
              mCallbackContext.error("Timeout");
            }
            break;
          }

          try {
            Thread.sleep(500);
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
        }
      }
    };

    watchImage.start();

    try {
      BitmapFactory.Options options = new BitmapFactory.Options();
      options.inScaled = false;
      byte[] decodedByte = Base64.decode(image, 0);
      Bitmap bitmap = BitmapFactory.decodeByteArray(decodedByte, 0, decodedByte.length);
      final int imgWidth = bitmap.getWidth();
      final int imgHeight = bitmap.getHeight();
      final int[] argb = new int[imgWidth * imgHeight];

      bitmap.getPixels(argb, 0, imgWidth, 0, 0, imgWidth, imgHeight);
      bitmap.recycle();

      mPrinter.printImage(argb, width, height, align, true);
      mPrinter.flush();

      mCallbackContext.success();
    } catch (Exception e) {
      System.out.println("[MAMA]: " + e.getMessage());
      mCallbackContext.error(this.getErrorByCode(11, e));
    }

    watchImage.interrupt();
  }

  /**
   * Exibe Toast de erro
   *
   * @param text
   * @param resetConnection
   */
  private void showError(final String text, boolean resetConnection) {
    //we'l ignore toasts at the moment
//        mCordova.getActivity().runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
//                Toast.makeText(mCordova.getActivity().getApplicationContext(), text, Toast.LENGTH_SHORT).show();
//            }
//        });
    if (resetConnection) {
      connect(mConnectCallbackContext);
    }
  }

  /**
   * Exibe mensagem Toast
   *
   * @param text
   */
  private void showToast(final String text) {
//        System.out.println("[PizzaPrinter]: " + text);
//
//        //we'l ignore toasts at the moment
//        mCordova.getActivity().runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
//                if (!mCordova.getActivity().isFinishing()) {
//                    Toast.makeText(mCordova.getActivity().getApplicationContext(), text, Toast.LENGTH_SHORT).show();
//                }
//            }
//        });
  }

  /**
   * Create a new plugin result and send it back to JavaScript
   *
   * @param
   */
  private void sendStatusUpdate(boolean isConnected, boolean hasPaper, boolean lowBattery) {
    final Intent intent = new Intent("PizzaPrinter.connectionStatus");

    Bundle b = new Bundle();
    b.putString( "userdata", "{ isConnected: "+ isConnected + ", hasPaper: "+ hasPaper + ", lowBattery: "+ lowBattery + "}" );

    intent.putExtras(b);

    LocalBroadcastManager.getInstance(mWebView.getContext()).sendBroadcastSync(intent);
  }

  private void sendStatusUpdate(boolean isConnected, boolean hasPaper) {
    this.sendStatusUpdate(isConnected, hasPaper, false);
  }

  private void sendStatusUpdate(boolean isConnected) {
    this.sendStatusUpdate(isConnected, true, false);
  }
}
