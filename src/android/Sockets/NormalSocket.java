package com.giorgiofellipe.pizzaprinter.Sockets;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;

import java.lang.reflect.Method;

/**
 * Created by Raymond on 4/21/2018.
 */

public class NormalSocket extends SocketType {
  public BluetoothSocket get(BluetoothDevice device) {
    try {
      Method method = device.getClass().getMethod("createRfcommSocket", new Class[]{int.class});

      return (BluetoothSocket) method.invoke(device, 1);
    } catch ( Exception e ) {
      return null;
    }
  }
}
