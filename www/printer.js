var exec = require('cordova/exec');

var printer = {
    platforms: ['android'],

    isSupported: function () {
        if (window.device) {
            var platform = window.device.platform;
            if ((platform !== undefined) && (platform !== null)) {
                return (this.platforms.indexOf(platform.toLowerCase()) >= 0);
            }
        }
        return false;
    },
    isOnline: function (address, onSuccess, onError) {
        exec(onSuccess, onError, 'PizzaPrinter', 'isOnline', [address]);
    },
    getUsbDevices: function (onSuccess, onError) {
        exec(onSuccess, onError, 'PizzaPrinter', 'getUsbDevices', []);
    },
    printUsb: function (text, address, options, onSuccess, onError) {
      exec(onSuccess, onError, 'PizzaPrinter', 'printUsb', [text, address, options]);
    },
    restartDevice: function(onSuccess, onError) {
        exec(onSuccess, onError, 'PizzaPrinter', 'restartDevice', []);
    },
    getDeviceUptime: function (onSuccess, onError) {
        exec(onSuccess, onError, 'PizzaPrinter', 'getDeviceUptime', []);
    },
    isWifiConnected: function (onSuccess, onError) {
        exec(onSuccess, onError, 'PizzaPrinter', 'isWifiConnected', []);
    },
    isDeviceCharging: function (onSuccess, onError) {
        exec(onSuccess, onError, 'PizzaPrinter', 'isDeviceCharging', []);
    },
    isInternetConnected: function(onSuccess, onError) {
        exec(onSuccess, onError, 'PizzaPrinter', 'isInternetConnected', []);
    },
    getBatteryPercentage: function (onSuccess, onError) {
        exec(onSuccess, onError, 'PizzaPrinter', 'getBatteryPercentage', []);
    },
    listBluetoothDevices: function (onSuccess, onError) {
        exec(onSuccess, onError, 'PizzaPrinter', 'listBluetoothDevices', []);
    },
    connect: function (address, restartBT, onSuccess, onError) {
        exec(onSuccess, onError, 'PizzaPrinter', 'connect', [address, restartBT]);
    },
    disconnect: function (address, onSuccess, onError) {
        exec(onSuccess, onError, 'PizzaPrinter', 'disconnect', [address]);
    },
    feedPaper: function (lines, onSuccess, onError) {
        exec(onSuccess, onError, 'PizzaPrinter', 'feedPaper', [lines]);
    },
    printText: function (address, text, charset, onSuccess, onError) {
        exec(onSuccess, onError, 'PizzaPrinter', 'printText', [address, text, charset]);
    },
    printSelfTest: function (onSuccess, onError) {
        exec(onSuccess, onError, 'PizzaPrinter', 'printSelfTest', []);
    },
    getStatus: function (onSuccess, onError) {
        exec(onSuccess, onError, 'PizzaPrinter', 'getStatus', []);
    },
    getTemperature: function (onSuccess, onError) {
        exec(onSuccess, onError, 'PizzaPrinter', 'getTemperature', []);
    },
    setBarcode: function (align, small, scale, hri, height, onSuccess, onError) {
        exec(onSuccess, onError, 'PizzaPrinter', 'setBarcode', [align, small, scale, hri, height]);
    },
    printBarcode: function (type, data, onSuccess, onError) {
        exec(onSuccess, onError, 'PizzaPrinter', 'printBarcode', [type, data]);
    },
    printQRCode: function (size, eccLv, data, onSuccess, onError) {
        exec(onSuccess, onError, 'PizzaPrinter', 'printQRCode', [size, eccLv, data]);
    },
    printImage: function (address, image, width, height, align, onSuccess, onError) {
        exec(onSuccess, onError, 'PizzaPrinter', 'printImage', [address, image, width, height, align]);
    },
    printImageUsb: function (address, image, width, height, align, onSuccess, onError) {
        exec(onSuccess, onError, 'PizzaPrinter', 'printImageUsb', [address, image, width, height, align]);
    },
    drawPageRectangle: function (x, y, width, height, fillMode, onSuccess, onError) {
        exec(onSuccess, onError, 'PizzaPrinter', 'drawPageRectangle', [x, y, width, height, fillMode]);
    },
    drawPageFrame: function (x, y, width, height, fillMode, thickness, onSuccess, onError) {
        exec(onSuccess, onError, 'PizzaPrinter', 'drawPageFrame', [x, y, width, height, fillMode, thickness]);
    },
    selectPageMode: function (onSuccess, onError) {
        exec(onSuccess, onError, 'PizzaPrinter', 'selectPageMode', []);
    },
    selectStandardMode: function (onSuccess, onError) {
        exec(onSuccess, onError, 'PizzaPrinter', 'selectStandardMode', []);
    },
    setPageRegion: function (x, y, width, height, direction, onSuccess, onError) {
        exec(onSuccess, onError, 'PizzaPrinter', 'setPageRegion', [x, y, width, height, direction]);
    },
    printPage: function (onSuccess, onError) {
        exec(onSuccess, onError, 'PizzaPrinter', 'printPage', []);
    },
    write: function (address, bytes, onSuccess, onError) {
        exec(onSuccess, onError, 'PizzaPrinter', 'write', [address, bytes]);
    },
    restartBluetooth(onSuccess, onError) {
        exec(onSuccess, onError, 'PizzaPrinter', 'restartBluetooth', []);
    },
    writeHex: function (address, hex, onSuccess, onError) {
        exec(onSuccess, onError, 'PizzaPrinter', 'writeHex', [address, hex]);
    }
};
module.exports = printer;
