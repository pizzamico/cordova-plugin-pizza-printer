package com.giorgiofellipe.pizzaprinter;

import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbEndpoint;
import android.hardware.usb.UsbInterface;
import android.hardware.usb.UsbManager;
import org.apache.cordova.CallbackContext;

public class PrinterCallback {
  UsbDevice device;
  UsbManager usbManager;
  UsbDeviceConnection connection;
  UsbEndpoint endPoint;
  CallbackContext callbackContext;

  PrinterCallback(UsbDevice device, UsbManager usbManager, CallbackContext callbackContext) {
    this.device = device;
    this.usbManager = usbManager;
    this.callbackContext = callbackContext;
  }

  public void begin() {

  }

  public void onFail() {

  }

  public void write(byte[] bytes) {
    connection.bulkTransfer(endPoint, bytes, bytes.length, 0);
  }

  public void write(byte[] bytes, int offset, int length) {
    connection.bulkTransfer(endPoint, bytes, length, 0);
  }

  public void onReady() {
    try {
      UsbInterface mInterface = device.getInterface(0);

      endPoint = mInterface.getEndpoint(1);// 0 IN and  1 OUT to printer.
      connection = usbManager.openDevice(device);

      connection.claimInterface(mInterface, true);

      Thread thread = new Thread(new Runnable() {
        @Override
        public void run() {
          begin();
        }
      });

      thread.run();
    } catch ( Exception e ) {
        this.callbackContext.error("error on ready");
    }
  }
}
