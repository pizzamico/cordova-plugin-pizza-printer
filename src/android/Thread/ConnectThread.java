package com.giorgiofellipe.pizzaprinter.Thread;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;

import com.giorgiofellipe.pizzaprinter.Sockets.SocketType;

import java.io.IOException;

/**
 * Created by Raymond on 5/3/2018.
 */

public abstract class ConnectThread extends Thread {
  private final BluetoothDevice device;

  public Exception exception = null;
  public BluetoothSocket socket = null;
  public SocketType socketType = null;

  public ConnectThread(BluetoothDevice inputDevice, SocketType inputSocketType) {
    socketType = inputSocketType;
    device = inputDevice;
  }

  public void onConnected(BluetoothSocket socket) {

  }

  public void onFailed() {

  }

  public void setException(Exception e) {
    e.printStackTrace();
    exception = e;
  }

  public void connected(BluetoothSocket bluetoothSocket) {
    System.out.println("[PizzaPrinter][ConnectThread] connection success! (" + socketType.getClass().getSimpleName() + ")");
    onConnected(bluetoothSocket);
  }

  public boolean failed = false;

  public void fail(Exception e) {
    if ( failed ) {
      return;
    }

    System.out.println("[PizzaPrinter][ConnectThread] connection failed (" + socketType.getClass().getSimpleName() + ")");

    failed = true;

    if ( socket != null ) {
      try {
        socket.close();
      } catch (IOException e1) {
        e1.printStackTrace();
      }
    }

    setException(e);
    onFailed();
  }

  public void begin() {
    System.out.println("[PizzaPrinter][ConnectThread] Attempting connection (" + socketType.getClass().getSimpleName() + ")");

    WatchThread watchThread = new WatchThread(this, 10000);
    watchThread.start();

    this.start();
  }

  public void run() {
    BluetoothAdapter.getDefaultAdapter().cancelDiscovery();
    BluetoothSocket tmp = socketType.get(device);

    if ( tmp == null ) {
      fail( new Exception("No connection") );
      return;
    }

    try {
      Thread.sleep(100);

      socket = tmp;
      socket.connect();

      connected(socket);
    } catch (Exception e) {
      fail( e );
    }
  }
}
