package com.giorgiofellipe.pizzaprinter.Sockets;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;

import java.lang.reflect.Method;
import java.util.UUID;

/**
 * Created by Raymond on 4/21/2018.
 */

public class ServiceRecordSocket extends SocketType {
  public UUID UUID_SPP = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

  public BluetoothSocket get(BluetoothDevice device) {
    try {
      Method method = device.getClass().getMethod("createRfcommSocketToServiceRecord", new Class[]{UUID.class});
      return (BluetoothSocket) method.invoke(device, UUID_SPP);
    } catch ( Exception e ) {
      return null;
    }
  }
}
