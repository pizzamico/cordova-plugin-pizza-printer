package com.giorgiofellipe.pizzaprinter.Thread;

/**
 * Created by Raymond on 6/9/2018.
 */

public class TimerThread extends Thread {
  public int seconds;

  public TimerThread(int inputSeconds) {
    seconds = inputSeconds;
  }

  public void timeOver() {

  }

  public void run() {
    long startTime = System.currentTimeMillis();

    while (true) {
      if ( Thread.interrupted() ) {
        break;
      }

      long curTime = System.currentTimeMillis();

      if ( (curTime - (seconds * 1000) ) > startTime ) {
        timeOver();
        break;
      }
    }
  }
}
