package com.giorgiofellipe.pizzaprinter;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.usb.*;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.json.JSONObject;
import org.json.JSONArray;

import android.content.Context;
import android.util.Base64;
import android.util.Log;

import java.io.IOException;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;

import javax.security.auth.callback.Callback;

public class PizzaUSBPrinter extends CordovaPlugin {
  static CordovaInterface cordova = null;

  public static void getUsbDevices(CallbackContext callbackContext) {
    UsbManager mUsbManager = (UsbManager) cordova.getActivity().getSystemService(Context.USB_SERVICE);

    HashMap<String, UsbDevice> mDeviceList = mUsbManager.getDeviceList();

    JSONArray json = new JSONArray();

    if (mDeviceList.size() > 0) {
      Iterator<UsbDevice> mDeviceIterator = mDeviceList.values().iterator();

      while (mDeviceIterator.hasNext()) {
        UsbDevice usbDevice = mDeviceIterator.next();

        Hashtable map = new Hashtable();

        try {
          map.put("device_id", usbDevice.getDeviceId());
          map.put("device_name", usbDevice.getDeviceName());
          map.put("product_name", usbDevice.getProductName().trim());
          map.put("vendor_id", usbDevice.getVendorId());
        } catch ( Exception e ) {

        }

        JSONObject jObj = new JSONObject(map);

        json.put(jObj);
      }
    }

    callbackContext.success(json);
  }



  public static UsbDevice getUsbDeviceById(String deviceName) {
    UsbManager mUsbManager = (UsbManager) cordova.getActivity().getSystemService(Context.USB_SERVICE);

    HashMap<String, UsbDevice> mDeviceList = mUsbManager.getDeviceList();

    if (mDeviceList.size() > 0) {
      Iterator<UsbDevice> mDeviceIterator = mDeviceList.values().iterator();

      while (mDeviceIterator.hasNext()) {
        UsbDevice usbDevice = mDeviceIterator.next();

        try {
          if (usbDevice.getProductName().trim().equalsIgnoreCase(deviceName)) {
            return usbDevice;
          }
        } catch ( Exception e ) {

        }
      }
    }

    return null;
  }

  public static void getPermission(UsbDevice device, final PrinterCallback printerCallback) {
    UsbManager usbManager = (UsbManager) cordova.getActivity().getSystemService(Context.USB_SERVICE);

    System.out.println("getting permission");
    if ( !usbManager.hasPermission(device) ) {

      System.out.println("getting permission2");

      final String ACTION_USB_PERMISSION = "com.android.example.USB_PERMISSION";

      BroadcastReceiver mUsbReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
          String action = intent.getAction();
          if (ACTION_USB_PERMISSION.equals(action)) {
            synchronized (this) {
              UsbDevice device = (UsbDevice) intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);

              if (intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false)) {
                if (device != null) {
                  printerCallback.onReady();
                  //call method to set up device communication
                }
              } else {
                System.out.println("permission denied for device " + device);
                printerCallback.onFail();
              }
            }
          }
        }
      };

      PendingIntent mPermissionIntent = PendingIntent.getBroadcast(cordova.getActivity(), 0, new Intent(ACTION_USB_PERMISSION), 0);
      IntentFilter filter = new IntentFilter(ACTION_USB_PERMISSION);

      cordova.getActivity().registerReceiver(mUsbReceiver, filter);

      usbManager.requestPermission(device, mPermissionIntent);
    } else {
      System.out.println("on ready");
      printerCallback.onReady();
    }
  }

  public static void printImage(String deviceName, final String image, final int width, final int height, final int align, final CallbackContext callbackContext) {
    UsbDevice device = getUsbDeviceById(deviceName);
    UsbManager mUsbManager = (UsbManager) cordova.getActivity().getSystemService(Context.USB_SERVICE);

    getPermission(device, new PrinterCallback(device, mUsbManager, callbackContext) {
      public void begin() {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inScaled = false;
        byte[] decodedByte = Base64.decode(image, 0);
        Bitmap bitmap = BitmapFactory.decodeByteArray(decodedByte, 0, decodedByte.length);
        final int imgWidth = bitmap.getWidth();
        final int imgHeight = bitmap.getHeight();
        final int[] argb = new int[imgWidth * imgHeight];

        bitmap.getPixels(argb, 0, imgWidth, 0, 0, imgWidth, imgHeight);
        bitmap.recycle();

        try {
          printImage2(argb, width, height, align, true, false, this);
        } catch ( Exception e ) {
          callbackContext.error(e.getMessage());
        }

        byte[] cut_paper = {0x1D, 0x56, 0x41, 0x10};
        this.write(cut_paper);

        callbackContext.success();
      }

      public void onFailed() {
        callbackContext.error("Error");
      }
    });
  }

  private static int cropImage(int[] grayscale, int width, int height) {
    int length = width * height;

    int offset;
    for(offset = 0; offset < length && grayscale[length - 1 - offset] >= 128; ++offset) {
      ;
    }

    int newHeight = height - offset / width;
    return newHeight;
  }

  public static void printImage2(int[] argb, int width, int height, int align, boolean dither, boolean crop, PrinterCallback printerCallback) throws IOException {
    Object buf = null;
    boolean bufOffs = false;
    if(argb == null) {
      throw new NullPointerException("The argb is null");
    } else if(align >= 0 && align <= 2) {
      if(width >= 1 && height >= 1) {
        Printer.convertARGBToGrayscale(argb, width, height);
        if(dither) {
          Printer.ditherImageByFloydSteinberg(argb, width, height);
        }

        if(crop) {
          height = cropImage(argb, width, height);
        }

        byte[] var14 = new byte[width * 3 + 9];
//        synchronized(this) {
          byte var15 = 0;
          int var16 = var15 + 1;
          var14[var15] = 27;
          var14[var16++] = 51;
          var14[var16++] = 24;

          printerCallback.write(var14, 0, var16);

          var15 = 0;
          var16 = var15 + 1;
          var14[var15] = 27;
          var14[var16++] = 97;
          var14[var16++] = (byte)align;
          var14[var16++] = 27;
          var14[var16++] = 42;
          var14[var16++] = 33;
          var14[var16++] = (byte)(width % 256);
          var14[var16++] = (byte)(width / 256);
          var14[var14.length - 1] = 10;
          int j = 0;

          for(int offs = 0; j < height; ++j) {
            int i;
            if(j > 0 && j % 24 == 0) {
              printerCallback.write(var14);

              for(i = var16; i < var14.length - 1; ++i) {
                var14[i] = 0;
              }
            }

            for(i = 0; i < width; ++offs) {
              var14[var16 + i * 3 + j % 24 / 8] |= (byte)((argb[offs] < 128?1:0) << 7 - j % 8);
              ++i;
            }
          }

          printerCallback.write(var14);
//        }
      } else {
        throw new IllegalArgumentException("The size of image is illegal");
      }
    } else {
      throw new IllegalArgumentException("The align is illegal");
    }
  }

  public static void print(final String text, final String deviceName, final String options, final CallbackContext callbackContext) {
    final UsbDevice device = getUsbDeviceById(deviceName);

    if ( device == null ) {
      callbackContext.error("no device found with name");
      return;
    }

    UsbManager mUsbManager = (UsbManager) cordova.getActivity().getSystemService(Context.USB_SERVICE);

    getPermission(device, new PrinterCallback(device, mUsbManager, callbackContext) {
        public void begin() {
          byte[] reset = {0x1B, 0x21, 0x03};
          byte[] format = { 27, 33, 0 };

          this.write(reset);
          this.write(format);

          if ( options.contains("big_text") ) {
//          byte[] format = { 27, 33, 0 };

            byte[] boldMedium = {0x1B, 0x21, 0x20};

            // Bold
//          format[2] = ((byte)(0x8 | format[2]));

            // Height
//          format[2] = ((byte)(0x10 | format[2]));

            // Width
//          format[2] = ((byte) (0x20 | format[2]));

            System.out.println("HEY OPTIONS BIG TEXT");

            this.write(boldMedium);
          }

          this.write(text.getBytes());

          byte[] cut_paper = {0x1D, 0x56, 0x41, 0x10};
          this.write(cut_paper);

          callbackContext.success();
        }
    });
  }
}
