package com.giorgiofellipe.pizzaprinter;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaWebView;
import org.json.JSONObject;
import org.json.JSONArray;
import org.json.JSONException;

import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.SystemClock;

import java.util.ArrayList;

public class PizzaPrinter extends CordovaPlugin {
  private enum Option {
    listBluetoothDevices,
    getUsbDevices,
    printUsb,
    isOnline,
    connect,
    disconnect,
    feedPaper,
    printText,
    getStatus,
    restartBluetooth,
    isDeviceCharging,
    isInternetConnected,
    getTemperature,
    setBarcode,
    printBarcode,
    printQRCode,
    printImage,
    printImageUsb,
    printLogo,
    printSelfTest,
    restartDevice,
    setPageRegion,
    getDeviceUptime,
    isWifiConnected,
    getBatteryPercentage,
    selectPageMode,
    selectStandardMode,
    drawPageRectangle,
    drawPageFrame,
    printPage,
    write,
    writeHex;
  }

  ArrayList<PizzaSDKWrapper> printers = new ArrayList<PizzaSDKWrapper>();

  CordovaWebView webView;
  CordovaInterface cordova;

  public void initialize(CordovaInterface inputCordova, CordovaWebView inputWebView) {
    super.initialize(inputCordova, inputWebView);

    webView = inputWebView;
    cordova = inputCordova;

    PizzaUSBPrinter.cordova = inputCordova;

    PizzaSDKWrapper defaultPrinter = new PizzaSDKWrapper(cordova);
    defaultPrinter.setWebView(webView);

    this.printers.add(defaultPrinter);
  }

  public PizzaSDKWrapper getDefaultPrinter() {
    return printers.get(0);
  }

  public PizzaSDKWrapper getPrinter(String address) {
    for ( PizzaSDKWrapper printer : printers ) {
      if ( printer.mAddress != null && printer.mAddress.equals(address) ) {
        return printer;
      }
    }

    PizzaSDKWrapper printer = new PizzaSDKWrapper(cordova);
    printer.setWebView(webView);
    printer.setAddress(address);

    this.printers.add( printer );

    System.out.println("[HEHEHE] CREATE NOW: " + printer.mAddress);

    return printer;
  }

  @Override
  public boolean execute(String action, final JSONArray args, final CallbackContext callbackContext) throws JSONException {
    PizzaSDKWrapper printer = getDefaultPrinter();
    printer.setCallbackContext(callbackContext);

    Option option = null;

    try {
      option = Option.valueOf(action);
    } catch (Exception e) {
      return false;
    }
    switch (option) {
      case listBluetoothDevices:
        printer.getBluetoothPairedDevices(callbackContext);
        break;
      case getUsbDevices:
        PizzaUSBPrinter.getUsbDevices(callbackContext);
        break;
      case printUsb:
        PizzaUSBPrinter.print(args.getString(0), args.getString(1), args.getString(2), callbackContext);
        break;
      case restartDevice:
        printer.restartDevice();
        break;
      case isOnline:
        String address = args.getString(0);
        printer.isOnline(address, callbackContext);
        break;
      case restartBluetooth:
        final PizzaSDKWrapper finalPrinter = printer;

        this.cordova.getThreadPool().execute(new Runnable() {
          @Override
          public void run() {
            finalPrinter.restartBluetooth();

            callbackContext.success(String.valueOf(true));
          }
        });
        break;
      case getBatteryPercentage:
        callbackContext.success(String.valueOf(printer.getBatteryPercentage(this.cordova.getActivity().getApplicationContext())));
        break;
      case isDeviceCharging:
        callbackContext.success(String.valueOf(printer.isCharging(this.cordova.getActivity().getApplicationContext())));
        break;
      case isInternetConnected:
        callbackContext.success(String.valueOf(printer.isInternetConnected()));
      case getDeviceUptime:
        callbackContext.success(Long.toString(SystemClock.elapsedRealtime()));
        break;
      case connect:
        printer = getPrinter(args.getString(0));
        printer.setCallbackContext(callbackContext);

        boolean restartBT = args.getBoolean(1);

        System.out.println("WE ARE RESTARTING BT: " + restartBT);

        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        if ( restartBT || !bluetoothAdapter.isEnabled() ) {
          printer.restartBluetooth();
        }

        System.out.println("[LMAO] connecting " + printer.mAddress );

        printer.connect(callbackContext);
        break;
      case isWifiConnected:
        final CordovaInterface cordova = this.cordova;

        this.cordova.getThreadPool().execute(new Runnable() {
          @Override
          public void run() {
            WifiManager wifiMgr = (WifiManager) cordova.getActivity().getApplicationContext().getSystemService(Context.WIFI_SERVICE);

            if (wifiMgr.isWifiEnabled()) { // Wi-Fi adapter is ON
              WifiInfo wifiInfo = wifiMgr.getConnectionInfo();

              if (wifiInfo.getNetworkId() == -1) {
                callbackContext.success(String.valueOf(false)); // Not connected to an access point
                return;
              }

              callbackContext.success(String.valueOf(true));

            } else {
              callbackContext.success(String.valueOf(false));
            }
          }
        });

        break;
      case disconnect:
        try {
          printer = getPrinter(args.getString(0));
          printer.setCallbackContext(callbackContext);

          printer.closeActiveConnections();
          callbackContext.success(PizzaUtil.getStringFromStringResource(this.cordova.getActivity().getApplication(), "printer_disconnected"));
        } catch (Exception e) {
          callbackContext.success(PizzaUtil.getStringFromStringResource(this.cordova.getActivity().getApplication(), "err_disconnect_printer"));
        }
        break;
      case feedPaper:
        printer.feedPaper(args.getInt(0));
        break;
      case printText:
        printer = getPrinter(args.getString(0));
        printer.setCallbackContext(callbackContext);

        String text = args.getString(1);
        String charset = args.getString(2);
        printer.printTaggedText(text, charset);
        break;
      case getStatus:
        printer.getStatus();
        break;
      case getTemperature:
        printer.getTemperature();
        break;
      case setBarcode:
        int align = args.getInt(0);
        boolean small = args.getBoolean(1);
        int scale = args.getInt(2);
        int hri = args.getInt(3);
        int height = args.getInt(4);
        printer.setBarcode(align, small, scale, hri, height);
        break;
      case printBarcode:
        int type = args.getInt(0);
        String data = args.getString(1);
        printer.printBarcode(type, data);
        break;
      case printQRCode:
        int size = args.getInt(0);
        int eccLv = args.getInt(1);
        data = args.getString(2);
        printer.printQRCode(size, eccLv, data);
        break;
      case printImageUsb:
        System.out.println("printImageUsb");

        this.cordova.getThreadPool().execute(new Runnable() {
            @Override
            public void run() {
                try {
                    PizzaUSBPrinter.printImage(args.getString(0), args.getString(1), args.getInt(2), args.getInt(3), args.getInt(4), callbackContext);
                } catch ( Exception e ) {
                    e.printStackTrace();
                  callbackContext.error("failed");
                }
            }
        });

        break;
      case printImage:
        this.cordova.getThreadPool().execute(new Runnable() {
          @Override
          public void run() {
            PizzaSDKWrapper printer = null;
            try {
              printer = getPrinter(args.getString(0));
              printer.setCallbackContext(callbackContext);

              System.out.println("[MAMA] PRINTING PRINT IMAGE: " + printer.mAddress);

              String image = args.getString(1);
              int imgWidth = args.getInt(2);
              int imgHeight = args.getInt(3);
              int imgAlign = args.getInt(4);
              printer.printImage(image, imgWidth, imgHeight, imgAlign);
            } catch (JSONException e) {
              e.printStackTrace();
            }
          }
        });

        break;
      case printLogo:
        break;
      case printSelfTest:
        printer.printSelfTest();
        break;
      case drawPageRectangle:
        int x = args.getInt(0);
        int y = args.getInt(1);
        int width = args.getInt(2);
        height = args.getInt(3);
        int fillMode = args.getInt(4);
        printer.drawPageRectangle(x, y, width, height, fillMode);
        break;
      case selectPageMode:
        printer.selectPageMode();
        break;
      case selectStandardMode:
        printer.selectStandardMode();
        break;
      case setPageRegion:
        x = args.getInt(0);
        y = args.getInt(1);
        width = args.getInt(2);
        height = args.getInt(3);
        int direction = args.getInt(4);
        printer.setPageRegion(x, y, width, height, direction);
        break;
      case drawPageFrame:
        x = args.getInt(0);
        y = args.getInt(1);
        width = args.getInt(2);
        height = args.getInt(3);
        fillMode = args.getInt(4);
        int thickness = args.getInt(5);
        printer.drawPageFrame(x, y, width, height, fillMode, thickness);
        break;
      case printPage:
        printer.printPage();
        break;
      case write:
        printer = getPrinter(args.getString(0));
        printer.setCallbackContext(callbackContext);

        byte[] bytes = args.getString(1).getBytes();
        printer.write(bytes);
        break;
      case writeHex:
        printer = getPrinter(args.getString(0));
        printer.setCallbackContext(callbackContext);

        String hex = args.getString(1);
        printer.writeHex(hex);
        break;
    }
    return true;
  }
}
